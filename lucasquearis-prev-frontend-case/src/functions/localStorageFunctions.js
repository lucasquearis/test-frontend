export function localStorageSet(response) {
  localStorage.setItem('token', JSON.stringify(response));
}

export function localStorageGet() {
  return JSON.parse(localStorage.getItem('token')) || {};
}
