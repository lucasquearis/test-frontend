import axios from 'axios';
import { localStorageGet } from './localStorageFunctions';

async function moduleFetch(value) {
  try {
    const { token } = localStorageGet();
    const headers = {
      'Content-Type': 'application/json',
      authorization: token,
    };
    const response = await axios.get(`http://localhost:8080/counter?tipo=${value}`, { headers });
    const [firstElement] = response.data;
    return firstElement;
  } catch (error) {
    return { error };
  }
}

export default moduleFetch;
