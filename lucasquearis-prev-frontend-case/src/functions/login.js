import axios from 'axios';

async function login(username, password) {
  try {
    const response = await axios.post('http://localhost:8080/oauth', {
      username,
      password,
    });
    return { response: response.data };
  } catch (error) {
    return { error: error.response.data };
  }
}

export default login;
