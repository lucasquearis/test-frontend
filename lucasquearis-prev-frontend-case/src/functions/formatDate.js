function formatDate(date) {
  const novaData = new Date(date);
  const dia = novaData.getDate();
  const mes = novaData.getMonth();
  const ano = novaData.getFullYear();
  const hora = novaData.getHours();
  const minutos = novaData.getMinutes();
  return `${dia}/${mes}/${ano} - ${hora}:${minutos}`;
}

export default formatDate;
