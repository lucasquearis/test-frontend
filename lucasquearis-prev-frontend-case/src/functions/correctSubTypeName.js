const correctSubTypeName = (subtype) => {
  switch (true) {
    case subtype === 'NORMAL':
      return 'Normal';
    case subtype === 'RURAL':
      return 'Rural';
    case subtype === 'NORMAL_PONTOS':
      return 'Normal Pontos';
    case subtype === 'PROFESSOR':
      return 'Professor';
    default:
      return subtype;
  }
};

export default correctSubTypeName;
