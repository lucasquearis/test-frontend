import jwtDecode from 'jwt-decode';

function userInfos(rawToken) {
  if (rawToken) {
    const userToken = rawToken.split(' ')[1];
    const { conta } = jwtDecode(userToken);
    return { ...conta, token: rawToken };
  }
  return {};
}

export default userInfos;
