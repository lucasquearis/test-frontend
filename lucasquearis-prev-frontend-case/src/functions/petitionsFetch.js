import axios from 'axios';
import { localStorageGet } from './localStorageFunctions';

async function petitionsFetch(page) {
  try {
    const { token } = localStorageGet();
    const headers = {
      'Content-Type': 'application/json',
      authorization: token,
    };
    const response = await axios.get(`http://localhost:8080/peticoes?_page=${page}&_limit=2`, { headers });
    return response.data;
  } catch (error) {
    return { error };
  }
}

export default petitionsFetch;
