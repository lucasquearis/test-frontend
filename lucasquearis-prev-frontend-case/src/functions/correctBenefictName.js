const correctBenefictsName = (beneficts) => {
  switch (true) {
    case beneficts === 'APOSENTADORIA_POR_IDADE':
      return 'Aposentadoria por idade';
    case beneficts === 'SALARIO_MATERNIDADE':
      return 'Salário maternidade';
    case beneficts === 'PENSAO_POR_MORTE':
      return 'Pensão por morte';
    case beneficts === 'APOSENTADORIA_ESPECIAL':
      return 'Aposentadoria especial';
    case beneficts === 'APOSENTADORIA_POR_TEMPO_DE_CONTRIBUICAO':
      return 'Aposentadoria por tempo de contribuição';
    case beneficts === 'AUXILIO_DOENCA':
      return 'Auxílio doença';
    case beneficts === 'AUXILIO_RECLUSAO':
      return 'Auxílio Reclusão';
    case beneficts === 'APOSENTADORIA_POR_INVALIDEZ':
      return 'Aposentadoria por inválidez';
    case beneficts === 'APOSENTADORIA_POR_TEMPO_DE_CONTRIBUICAO_PROFESSOR':
      return 'Aposentadoria por tempo de contribuição professor';
    case beneficts === 'ASSISTENCIAL':
      return 'Assistencial';
    case beneficts === 'AUXILIO_ACIDENTE':
      return 'Auxílio acidente';
    case beneficts === 'APOSENTADORIA_POR_INCAPACIDADE':
      return 'Aposentadoria por incapacidade';
    default:
      return beneficts;
  }
};

export default correctBenefictsName;
