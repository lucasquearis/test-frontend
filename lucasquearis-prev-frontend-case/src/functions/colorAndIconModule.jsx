import React from 'react';
import ModuleCard from '../Components/ModuleCard';
import clientsIcon from '../Icons/clients-icon.png';
import calculationsIcon from '../Icons/calculations-icon.png';
import petitionsIcon from '../Icons/petition-icon.png';
import casesIcon from '../Icons/cases-icon.png';

const colorAndIconModule = (moduleName) => {
  let image = clientsIcon;
  let name = moduleName;
  let fontColor = '';
  switch (true) {
    case moduleName === 'CLIENTES':
      name = 'Clientes';
      image = clientsIcon;
      fontColor = '#3578C5';
      break;
    case moduleName === 'CALCULOS':
      name = 'Cálculos';
      image = calculationsIcon;
      fontColor = '#4CA881';
      break;
    case moduleName === 'PETICOES':
      name = 'Petições';
      image = petitionsIcon;
      fontColor = '#EE8753';
      break;
    case moduleName === 'CASOS':
      name = 'Casos';
      image = casesIcon;
      fontColor = '#6838B7';
      break;
    default:
      break;
  }
  return (
    <ModuleCard
      key={moduleName}
      moduleName={moduleName}
      title={name}
      image={image}
      fontColor={fontColor}
    />
  );
};

export default colorAndIconModule;
