import React from 'react';
import { screen, fireEvent } from '@testing-library/react';
import renderWithRouter from '../renderWithRouter';
import App from '../../App';
// import axios, {  } from 'axios'

describe('Testa Página de login', () => {
  beforeEach(() => {
    renderWithRouter(<App />);
  });

  it('Verifica se rota da página é redirecionada para /login', () => {
    expect(global.window.location.pathname).toEqual('/login');
  });

  it('Verifica se contem todos os elementos da página', () => {
    const logoImage = screen.getByRole('img', { name: 'Previdenciarista' });
    const labelEmailOrCpf = screen.getByText('Email ou CPF');
    const inputEmailOrCpf = screen.getByPlaceholderText('Digite seu email ou CPF');
    const labelSenha = screen.getByText('Senha');
    const inputSenha = screen.getByPlaceholderText('Digite sua senha');
    const buttonEsqueceuSenha = screen.getByRole('button', { name: 'Esqueceu a senha?' });
    const buttonEntrar = screen.getByRole('button', { name: 'Entrar' });

    expect(logoImage).toBeInTheDocument();
    expect(labelEmailOrCpf).toBeInTheDocument();
    expect(inputEmailOrCpf).toBeInTheDocument();
    expect(labelSenha).toBeInTheDocument();
    expect(inputSenha).toBeInTheDocument();
    expect(buttonEsqueceuSenha).toBeInTheDocument();
    expect(buttonEntrar).toBeInTheDocument();
  });

  it('Verifica se botão Entrar, está desabilitado', () => {
    const buttonEntrar = screen.getByRole('button', { name: 'Entrar' });
    expect(buttonEntrar).toBeDisabled();
  });

  it('Verifica se botão Entrar, está habilitado', () => {
    const buttonEntrar = screen.getByRole('button', { name: 'Entrar' });
    const inputEmailOrCpf = screen.getByPlaceholderText('Digite seu email ou CPF');
    const inputSenha = screen.getByPlaceholderText('Digite sua senha');

    fireEvent.change(inputEmailOrCpf, { target: { value: '12345' } });
    expect(inputEmailOrCpf).toHaveValue('12345');
    expect(buttonEntrar).toBeDisabled();
    fireEvent.change(inputSenha, { target: { value: '12345678' } });
    expect(buttonEntrar).not.toBeDisabled();
  });

  it('Verifica se é renderizado modal ao errar login', () => {
    const buttonEntrar = screen.getByRole('button', { name: 'Entrar' });
    const inputEmailOrCpf = screen.getByPlaceholderText('Digite seu email ou CPF');
    const inputSenha = screen.getByPlaceholderText('Digite sua senha');

    fireEvent.change(inputEmailOrCpf, { target: { value: '12345' } });
    expect(inputEmailOrCpf).toHaveValue('12345');
    expect(buttonEntrar).toBeDisabled();
    fireEvent.change(inputSenha, { target: { value: '12345678' } });
    expect(inputSenha).toHaveValue('12345678');
    expect(buttonEntrar).not.toBeDisabled();
    fireEvent.click(buttonEntrar);
    // const modalMessage = screen
    //   .getByRole('paragraph', { name: 'Erro ao tentar efeturar login' });
    // expect(modalMessage).toBeInTheDocument();

    // Ant Design renderiza componentes fora do APP
  });

  it('Verifica se é feito login com sucesso', () => {
    const buttonEntrar = screen.getByRole('button', { name: 'Entrar' });
    const inputEmailOrCpf = screen.getByPlaceholderText('Digite seu email ou CPF');
    const inputSenha = screen.getByPlaceholderText('Digite sua senha');

    fireEvent.change(inputEmailOrCpf, { target: { value: 'teste' } });
    expect(inputEmailOrCpf).toHaveValue('teste');
    expect(buttonEntrar).toBeDisabled();
    fireEvent.change(inputSenha, { target: { value: 'frontend' } });
    expect(inputSenha).toHaveValue('frontend');
    expect(buttonEntrar).not.toBeDisabled();
    fireEvent.click(buttonEntrar);
    // expect(global.window.location.pathname).toEqual('/dashboard');
  });
});
