import React from 'react';
import { render } from '@testing-library/react';
import { ConfigProvider } from 'antd';
import { BrowserRouter } from 'react-router-dom';
import ptBr from 'antd/lib/locale/pt_BR';
import { arrayOf, node, oneOfType } from 'prop-types';
import LoginProvider from '../context/LoginProvider';
import DashboardProvider from '../context/DashboardProvider';

// https://testing-library.com/docs/react-testing-library/setup#custom-render

function AllProviders({ children }) {
  return (
    <BrowserRouter>
      <ConfigProvider locale={ptBr}>
        <LoginProvider>
          <DashboardProvider>
            {children}
          </DashboardProvider>
        </LoginProvider>
      </ConfigProvider>
    </BrowserRouter>
  );
}

AllProviders.propTypes = {
  children: oneOfType([
    arrayOf(node),
    node,
  ]).isRequired,
};

const renderWithRouter = (ui, { route = '/' } = {}) => {
  window.history.pushState({}, 'Test page', route);

  return render(ui, { wrapper: AllProviders });
};

export default renderWithRouter;
