import { useState } from 'react';

function useUsername(initialState) {
  const [isValidUsername, setIsValidUsername] = useState(false);
  const [username, setUsername] = useState(initialState);

  const changeEvent = ({ target: { value } }) => {
    setUsername(value);
    return value.length > 4 ? setIsValidUsername(true) : setIsValidUsername(false);
  };

  return [username, changeEvent, isValidUsername];
}

export default useUsername;
