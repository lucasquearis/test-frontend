import { useState } from 'react';

function usePassword(initialState) {
  const [isValidPassword, setIsValidPassword] = useState(false);
  const [password, setPassword] = useState(initialState);

  const changeEvent = ({ target: { value } }) => {
    setPassword(value);
    return value.length > 7 ? setIsValidPassword(true) : setIsValidPassword(false);
  };

  return [password, changeEvent, isValidPassword];
}

export default usePassword;
