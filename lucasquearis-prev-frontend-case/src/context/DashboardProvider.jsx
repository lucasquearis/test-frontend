import { arrayOf, node, oneOfType } from 'prop-types';
import React, {
  useMemo, useState,
} from 'react';
import { localStorageGet } from '../functions/localStorageFunctions';
import DashboardContext from './DashboardContext';

function DashboardProvider({ children }) {
  const [user, setUser] = useState(localStorageGet());
  const [isModalAvatarOpen, setIsModalAvatarOpen] = useState(false);
  const [isModalPetitionOpen, setIsModalPetitionOpen] = useState(false);
  const [currentPetition, setCurrentPetition] = useState({});

  const context = useMemo(() => ({
    user,
    setUser,
    isModalAvatarOpen,
    setIsModalAvatarOpen,
    isModalPetitionOpen,
    setIsModalPetitionOpen,
    currentPetition,
    setCurrentPetition,
  }), [isModalAvatarOpen, user, isModalPetitionOpen]);

  return (
    <DashboardContext.Provider value={context}>
      {children}
    </DashboardContext.Provider>
  );
}

DashboardProvider.propTypes = {
  children: oneOfType([
    arrayOf(node),
    node,
  ]).isRequired,
};

export default DashboardProvider;
