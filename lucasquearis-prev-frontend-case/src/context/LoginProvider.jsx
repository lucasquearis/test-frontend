import React, { useMemo, useState } from 'react';
import { oneOfType, node, arrayOf } from 'prop-types';
import LoginContext from './LoginContext';

function LoginProvider({ children }) {
  const [error, setError] = useState('');
  const [isModalErrorVisible, setIsModalErrorVisible] = useState(false);
  const context = useMemo(() => ({
    setError,
    error,
    isModalErrorVisible,
    setIsModalErrorVisible,
  }), [error, isModalErrorVisible]);
  return (
    <LoginContext.Provider value={context}>
      {children}
    </LoginContext.Provider>
  );
}

LoginProvider.propTypes = {
  children: oneOfType([
    arrayOf(node),
    node,
  ]).isRequired,
};

export default LoginProvider;
