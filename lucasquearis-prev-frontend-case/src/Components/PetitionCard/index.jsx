import { arrayOf, shape, string } from 'prop-types';
import { v4 as uuidv4 } from 'uuid';
import React, { useContext } from 'react';
import { Button } from 'antd';
import {
  CardContainer,
  BenefitTag,
  PetitionInfo,
  SubTypeTitle,
  PetitionResume,
  PetitionFooter,
  PetitionDate,
  MoreInfoPetition,
  EyeIcon,
  BenefitTitle,
} from './style';
import formatDate from '../../functions/formatDate';
import DashboardContext from '../../context/DashboardContext';
import correctBenefictsName from '../../functions/correctBenefictName';
import correctSubTypeName from '../../functions/correctSubTypeName';

function PetitionCard({ petitionInfo }) {
  const {
    tiposDeBeneficio, subtipo, resumo, dataDeCriacao,
  } = petitionInfo;
  const { setIsModalPetitionOpen, setCurrentPetition } = useContext(DashboardContext);

  const openModal = () => {
    setCurrentPetition(petitionInfo);
    setIsModalPetitionOpen(true);
  };

  return (
    <CardContainer>
      <BenefitTag>
        {tiposDeBeneficio.map((benefict) => (
          <BenefitTitle key={uuidv4()}>{correctBenefictsName(benefict)}</BenefitTitle>
        ))}
      </BenefitTag>
      <PetitionInfo>
        {subtipo.map((subtype) => (
          <SubTypeTitle key={uuidv4()}>{correctSubTypeName(subtype)}</SubTypeTitle>
        ))}
        <PetitionResume>{resumo}</PetitionResume>
      </PetitionInfo>
      <PetitionFooter>
        <PetitionDate>
          {`Publicação: ${formatDate(dataDeCriacao)}`}
        </PetitionDate>
        <MoreInfoPetition>
          <Button onClick={openModal} type="link">
            <EyeIcon />
            Pré-visualizar petição
          </Button>
        </MoreInfoPetition>
      </PetitionFooter>
    </CardContainer>
  );
}

PetitionCard.propTypes = {
  petitionInfo: shape(
    {
      tiposDeBeneficio: arrayOf(string).isRequired,
      subtipo: arrayOf(string).isRequired,
      resumo: string.isRequired,
      dataDeCriacao: string.isRequired,
    },
  ).isRequired,
};

export default PetitionCard;
