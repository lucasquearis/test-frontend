import styled from 'styled-components';
import { EyeOutlined } from '@ant-design/icons';

export const CardContainer = styled.li`
  display: flex;
  flex-direction: column;
  box-shadow: 0px 1px 15px rgba(99, 99, 99, 0.2);
  background-color: white;
  padding: 15px;
  margin: 20px 0;
`;

export const BenefitTag = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  height: max-content;
`;

export const BenefitTitle = styled.h1`
  background-color: red;
  font-size: 16px;
  margin-right: 10px;
  padding: 4px 8px;
  border-radius: 5px;
  background-color: #EDF6FD;
  color: #3575AB;
  font-weight: 600;
`;

export const PetitionInfo = styled.div`

`;

export const SubTypeTitle = styled.h1`
  color: #ED7840;
  font-size: 16px;
  font-weight: 600;
`;

export const PetitionResume = styled.p`
  font-size: 16px;
  font-weight: 600;
`;

export const PetitionFooter = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-end;
`;

export const PetitionDate = styled.p`
  margin: 0;
  font-size: 16px;
  color: #3575AB;
`;

export const MoreInfoPetition = styled.div`
`;

export const EyeIcon = styled(EyeOutlined)`
  /* style={{ width: '15,57px', height: '11,64px', color: '#4E86B5' }} */
  width: 15.57px;
  height: 11.64px;
  color: #4E86B5;
`;
