import { string } from 'prop-types';
import React, { useEffect, useState } from 'react';
import moduleFetch from '../../functions/moduleFetch';
import { CardBox, HeaderCard, BodyCard } from './style';

function ModuleCard({
  moduleName, image, title, fontColor,
}) {
  const [result, setResult] = useState('');
  const [isLoading, setIsLoading] = useState(true);
  useEffect(async () => {
    setResult(await moduleFetch(moduleName));
    setIsLoading(false);
  }, []);

  return isLoading ? <h1>Loading</h1> : (
    <CardBox>
      <HeaderCard>
        <img src={image} alt={moduleName} />
        <p>{title}</p>
      </HeaderCard>
      <BodyCard fontColor={fontColor}>
        <h1>{result.total}</h1>
        <p>{`Este mês: ${result.totalPeriodo.mensal}`}</p>
      </BodyCard>
    </CardBox>
  );
}

ModuleCard.propTypes = {
  moduleName: string.isRequired,
  image: string.isRequired,
  title: string.isRequired,
  fontColor: string.isRequired,
};

export default ModuleCard;
