import styled from 'styled-components';

export const CardBox = styled.li`
  width: 168.23px;
  height: 128px;
  background-color: white;
  display: flex;
  flex-direction: column;
  box-shadow: 0px 1px 15px rgba(99, 99, 99, 0.2);
  padding: 5px 0;
  margin: 8px;
`;

export const HeaderCard = styled.div`
  display: flex;
  margin: 5px 15px;
  img {
    width: 24px;
  };
  p {
    margin: 0;
    font-size: 16px;
    padding-left: 5px;
  };
`;

export const BodyCard = styled.div`
  display: flex;
  flex-direction: column;
  margin: 5px 15px;

  h1 {
    font-size: 38px;
    margin: 0;
    color: ${(props) => props.fontColor};
    font-weight: 600;
  }
  p {
    font-size: 14px;
    font-weight: 400;
    color: #949595;
    margin: 0;
  }
`;
