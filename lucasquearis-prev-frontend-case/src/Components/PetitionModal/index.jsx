import { Modal } from 'antd';
import React, { useContext } from 'react';
import DashboardContext from '../../context/DashboardContext';
import formatDate from '../../functions/formatDate';

function PetitionModal() {
  const {
    isModalPetitionOpen,
    setIsModalPetitionOpen,
    currentPetition,
  } = useContext(DashboardContext);

  const {
    atualizadoPor, carenciaMinima, competencia, criadoPor, criticas, curtidas,
    dataDaUltimaAtualizacao, dataDeCriacao, idadeMinima, periodo, resumo, score, sexo,
    status, subtipo, tags, tipo, tipoDeAcao, tipoDeProcesso, tiposDeBeneficio, titulo,
  } = currentPetition;

  return (
    <Modal
      title={titulo}
      visible={isModalPetitionOpen}
      onOk={() => setIsModalPetitionOpen(false)}
      onCancel={() => setIsModalPetitionOpen(false)}
      cancelButtonProps={{ style: { display: 'none' } }}
    >
      <p>{`Status: ${status}`}</p>
      <p>{`Atualizado por: ${atualizadoPor}`}</p>
      <p>{`Carência mínima: ${carenciaMinima}`}</p>
      <p>{`Competência: ${competencia}`}</p>
      <p>{`Criado por: ${criadoPor}`}</p>
      <p>{`Número de críticas: ${criticas}`}</p>
      <p>{`Número de curtidas: ${curtidas}`}</p>
      <p>{`Data da última atualização: ${formatDate(dataDaUltimaAtualizacao)}`}</p>
      <p>{`Data de criação: ${formatDate(dataDeCriacao)}`}</p>
      <p>{`Idade mínima: ${idadeMinima}`}</p>
      <p>{`Período: ${periodo}`}</p>
      <p>{`Resumo: ${resumo}`}</p>
      <p>{`Score: ${score}`}</p>
      <p>{`Gênero: ${sexo}`}</p>
      <p>{`Subtipo: ${subtipo.map((item) => item)}`}</p>
      <p>{`Tags: ${tags.map((item) => item)}`}</p>
      <p>{`Tipo: ${tipo}`}</p>
      <p>{`Tipo de ação: ${tipoDeAcao}`}</p>
      <p>{`Tipo de processo: ${tipoDeProcesso}`}</p>
      <p>{`Tipos de benefícios: ${tiposDeBeneficio.map((item) => item)}`}</p>

    </Modal>
  );
}

export default PetitionModal;
