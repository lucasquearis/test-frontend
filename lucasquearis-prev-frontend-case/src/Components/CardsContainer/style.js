import styled from 'styled-components';

export const CardsUl = styled.ul`
  display: flex;
  justify-content: space-evenly;
  padding: 0;
  margin: 10px 0 40px 0;
  flex-wrap: wrap;
`;

export default CardsUl;
