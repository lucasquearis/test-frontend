import React, { useContext } from 'react';
import { CardsUl } from './style';
import DashboardContext from '../../context/DashboardContext';
import colorAndIconModule from '../../functions/colorAndIconModule';

function CardsContainer() {
  const { user } = useContext(DashboardContext);

  return (
    <CardsUl>
      {user.modulos ? (user.modulos
        .map((moduleName) => colorAndIconModule(moduleName))) : <p>Loading</p>}
    </CardsUl>
  );
}

export default CardsContainer;
