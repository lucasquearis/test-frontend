import React, { useContext } from 'react';
import { Modal } from 'antd';
import { useNavigate } from 'react-router-dom';
import DashboardContext from '../../context/DashboardContext';

function AvatarModal() {
  const navigate = useNavigate();
  const { isModalAvatarOpen, setIsModalAvatarOpen, user } = useContext(DashboardContext);

  const logOut = () => {
    setIsModalAvatarOpen(false);
    localStorage.removeItem('token');
    navigate('/login');
  };

  return (
    <Modal
      title="Informações do Usuário"
      visible={isModalAvatarOpen}
      onOk={logOut}
      onCancel={() => setIsModalAvatarOpen(false)}
      cancelButtonProps={{ style: { display: 'none' } }}
      okText="Deslogar"
    >
      <img src={user.urlImagemPerfil} width="100%" alt={user.nome} />
      <p>{`Nome: ${user.nome}`}</p>
      <p>{`Privilégio: ${user.privilegio}`}</p>
      <p>{`E-mail: ${user.email}`}</p>
    </Modal>
  );
}

export default AvatarModal;
