import styled from 'styled-components';

export const PetitionsBox = styled.div`
  .title-petitions {
    font-size: 20px;
    font-weight: 600;
  }
`;

export const PaginationBody = styled.div`
  display: flex;
  justify-content: end;
`;

export const PetitionsUl = styled.ul`
  padding: 0;
  list-style: none;
`;
