import React, { useCallback, useEffect, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { Pagination } from 'antd';
import { PaginationBody, PetitionsBox, PetitionsUl } from './style';
import petitionsFetch from '../../functions/petitionsFetch';
import PetitionCard from '../PetitionCard';

function PetitionsContainer() {
  const [page, setPage] = useState(1);
  const [currentPetitions, setCurrentPetitions] = useState([]);

  useEffect(useCallback(async () => {
    const response = await petitionsFetch(page);
    setCurrentPetitions(response);
  }), [page]);

  return (
    <PetitionsBox>
      <h1 className="title-petitions">Últimas petições</h1>
      <PetitionsUl>
        {currentPetitions.map((petition) => (
          <PetitionCard
            key={uuidv4()}
            petitionInfo={petition}
          />
        ))}
      </PetitionsUl>
      <PaginationBody>
        <Pagination
          onChange={setPage}
          defaultCurrent={1}
          pageSize={2}
          total={32}
        />
      </PaginationBody>
    </PetitionsBox>
  );
}

export default PetitionsContainer;
