import React, { useContext } from 'react';
import {
  Form, Input, Button,
} from 'antd';
import { useNavigate } from 'react-router-dom';
import prevIcon from '../../Icons/prev-icon.png';
import {
  StyleLoginBtn, LoginContainer, LoginBox, StyleLogo,
} from './style';
import useUsername from '../../Hooks/useUsername';
import usePassword from '../../Hooks/usePassword';
import login from '../../functions/login';
import LoginContext from '../../context/LoginContext';
import { localStorageSet } from '../../functions/localStorageFunctions';
import DashboardContext from '../../context/DashboardContext';
import userInfos from '../../functions/userInfos';

function LoginContainerComponent() {
  const [username, changeEventUsername, isValidUsername] = useUsername('');
  const [password, changeEventPassword, isValidPassword] = usePassword('');
  const { setError, setIsModalErrorVisible } = useContext(LoginContext);
  const { setUser } = useContext(DashboardContext);
  const navigate = useNavigate();

  const makeLogin = async () => {
    const response = await login(username, password);
    if (response.error) {
      setError(response.error);
      setIsModalErrorVisible(true);
    }
    if (response.response) {
      const { access_token: acessToken, token_type: tokenType } = response.response;
      const formatToken = `${tokenType} ${acessToken}`;
      localStorageSet(userInfos(formatToken));
      setUser(userInfos(formatToken));
      navigate('/dashboard');
    }
  };

  return (
    <LoginContainer>
      <LoginBox>
        <StyleLogo src={prevIcon} alt="Previdenciarista" />
        <Form layout="vertical" onFinish={makeLogin}>
          <Form.Item label="Email ou CPF">
            <Input
              style={{ borderColor: isValidUsername ? '#3575AB' : 'red' }}
              value={username}
              onChange={changeEventUsername}
              placeholder="Digite seu email ou CPF"
            />
          </Form.Item>
          <Form.Item label="Senha">
            <Input.Password
              style={{ borderColor: isValidPassword ? '#3575AB' : 'red' }}
              value={password}
              onChange={changeEventPassword}
              placeholder="Digite sua senha"
            />
          </Form.Item>
          <Form.Item>
            <Button style={{ padding: '0px' }} type="link">Esqueceu a senha?</Button>
          </Form.Item>
          <Form.Item>
            <StyleLoginBtn
              disabled={!isValidUsername || !isValidPassword}
              htmlType="submit"
              type="primary"
              block
            >
              Entrar
            </StyleLoginBtn>
          </Form.Item>
        </Form>
      </LoginBox>
    </LoginContainer>
  );
}

export default LoginContainerComponent;
