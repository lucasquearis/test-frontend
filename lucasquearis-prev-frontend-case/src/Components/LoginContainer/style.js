import styled from 'styled-components';
import { Button } from 'antd';

export const LoginContainer = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const LoginBox = styled.div`
  width: 336px;
`;

export const StyleLoginBtn = styled(Button)`
  &:hover, :focus{
    background-color: #F26526;
    border-color: #F26526;
  }
  background-color: #F26526;
  border-color: #F26526;
  height: 40px;
  border-radius: 4px;
`;

export const StyleLogo = styled.img`
  margin: 15px 0
`;
