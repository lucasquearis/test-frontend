import styled from 'styled-components';

export const Dashboard = styled.div`
  height: 100vh;
  width: 100vw;
  padding: 30px;
`;

export const Header = styled.div`
  height: max-content;
  width: 100%;
  display: flex;
  justify-content: space-between;
  padding: 0 5px;
`;

export const StyleLogo = styled.img`
  margin: 15px 0
`;

export const AvatarIcon = styled.img`
  max-height: 55px;
  border-radius: 50px;
`;

export const AvatarButton = styled.button`
  background-color: white;
  border-style: none;
  cursor: pointer;
`;
