import React, {
  useContext,
} from 'react';
import {
  Dashboard, Header, StyleLogo, AvatarIcon, AvatarButton,
} from './style';
import prevIcon from '../../Icons/prev-icon.png';
import DashboardContext from '../../context/DashboardContext';
import AvatarModal from '../AvatarModal';
import CardsContainer from '../CardsContainer';
import PetitionsContainer from '../PetitionsContainer';
import PetitionModal from '../PetitionModal';

function DashboardContainer() {
  const {
    user,
    isModalAvatarOpen,
    setIsModalAvatarOpen,
    isModalPetitionOpen,
  } = useContext(DashboardContext);

  return (
    <>
      <Dashboard>
        <Header>
          <StyleLogo src={prevIcon} alt="Previdenciarista" />
          <AvatarButton type="button" onClick={() => setIsModalAvatarOpen(true)}>
            {user.urlImagemPerfil && <AvatarIcon src={user.urlImagemPerfil} alt="Avatar" />}
          </AvatarButton>
        </Header>
        <CardsContainer />
        <PetitionsContainer />
      </Dashboard>
      {isModalAvatarOpen && <AvatarModal />}
      {isModalPetitionOpen && <PetitionModal />}
    </>
  );
}

export default DashboardContainer;
