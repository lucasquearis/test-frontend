import React, { useContext } from 'react';
import { Modal } from 'antd';
import LoginContext from '../../context/LoginContext';

function ErrorModal() {
  const { error, setIsModalErrorVisible, isModalErrorVisible } = useContext(LoginContext);

  return (
    <Modal
      cancelButtonProps={{ style: { display: 'none' } }}
      title="Erro ao tentar efeturar login"
      visible={isModalErrorVisible}
      onOk={() => setIsModalErrorVisible(false)}
      onCancel={() => setIsModalErrorVisible(false)}
    >
      <p>{error.message}</p>
    </Modal>
  );
}

export default ErrorModal;
