import React, { useContext, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import ErrorModal from '../../Components/ErrorModal';
import LoginContainerComponent from '../../Components/LoginContainer';
import LoginContext from '../../context/LoginContext';
import { localStorageGet } from '../../functions/localStorageFunctions';

function Login() {
  const { error } = useContext(LoginContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (localStorageGet().urlImagemPerfil) {
      navigate('/dashboard');
    }
  }, []);

  return (
    <div>
      <LoginContainerComponent />
      {error && <ErrorModal />}
    </div>
  );
}

export default Login;
