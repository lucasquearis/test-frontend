import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import DashboardContainer from '../../Components/DashboardContainer';
import { localStorageGet } from '../../functions/localStorageFunctions';

function Dashboard() {
  const navigate = useNavigate();

  useEffect(() => {
    if (!localStorageGet().urlImagemPerfil) {
      navigate('/login');
    }
  }, []);

  return (
    <DashboardContainer />
  );
}

export default Dashboard;
