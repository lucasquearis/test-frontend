import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { ConfigProvider } from 'antd';
import ptBr from 'antd/lib/locale/pt_BR';
import App from './App';
import 'antd/dist/antd.css';
import LoginProvider from './context/LoginProvider';
import DashboardProvider from './context/DashboardProvider';

render(
  <BrowserRouter>
    <ConfigProvider locale={ptBr}>
      <LoginProvider>
        <DashboardProvider>
          <App />
        </DashboardProvider>
      </LoginProvider>
    </ConfigProvider>
  </BrowserRouter>,
  document.getElementById('root'),
);
