# Bem vindo(a) ao repositorio do meu desafio técnico para empresa Previdenciarista

O case tem o objetivo de avaliar os conhecimentos técnicos para a vaga de Front-end Developer.
Sendo avaliado nos seguintes tópicos:

- ContextApi, Hooks, States, Routes.
- Requisições RESTFul.
- Styled-Components para customização.
- Componentização, reutilização e extensibilidade dos componentes visuais.
- Organização, semântica, estrutura, legibilidade, manutenibilidade do seu código.
- Código limpo.

# Sumário

- [Diferênciais](#diferenciais)
- [Diretrizes](#diretrizes)
- [Requisitos](#requisitos)
- [Iniciando a aplicação Localmente](#initApp)
- [Arquitetura](#architecture)
- [Testes](#testes)
- [Tecnologias utilizadas](#technologies)

## <a name="diferenciais"></a> Diferenciais

- Testes unitários / funcionais.
- Commits semanticos.
- Adaptar a página para dispositivos móveis (torná-la responsiva).
- Boa documentação.


## <a name="diretrizes"></a> Diretrizes

- Deve ser utilizado o <code>creact-react-app</code> com a versão mais atual.
- Seguir a implementação do Design [Figma](https://www.figma.com/file/RCZ9wWsHBet9dTQl6RIIHW/Teste-Prev-Front-end) chegando mais próximo possível.
- Utilizar o framework [AntD](https://ant.design/) para base dos componentes
- As chamadas aos serviços esta disponível no servidor mock <code>http://localhost:8080</code>

## <a name="requisitos"></a> Requisitos

- Login.
- Criar a tela e o fluxo de login.
  - rota para login <code>POST</code> <code>http://localhost:8080/</code> oauth passando no request <code>{ "username": "teste", "password": "frontend" }</code>.
- Redirecionar para rota <code>/dashboard</code>.
- Dashboard.
  - Acessível somente para usuário logado, se não retornar ao login.
  - Carregar as informações do usuário logado.
  - As informações do usuário são retornados no <code>access_token</code> no formato JWT.
  - Clicar sobre o avatar do usuário abrir um Modal mostrando as informações do usuário logado.
- Mostrar os contadores com base na info do usuário logado.
  - rota para os contadores <code>GET</code> <code>/counter?tipo={tipoModulo}</code>.
- Carregar uma lista de petições.
  - A lista deve ser páginada de 2 em 2 resultados, mostrando uma paginação.
  Ao clicar em "Pré-visualizar petição", abrir uma modal para mostrar as informações detalhadas da petição, esta tela você esta livre para mostrar os detalhes da petição como achar melhor! #### Atenção:
  - Para realizar a paginação do serviço deve ser passado os QueryParams <code>?_page=1&_limit=2</code>.
  - O total de item do serviço <code>/peticoes?_page=1&_limit=2</code> retorna no header <code>X-Total-Count</code> o total de petições cadastradas
  - <code>https://github.com/typicode/json-server#paginate</code>

## <a name="initApp"></a> Iniciando a Aplicação Localmente

Para iniciar a aplicação primeiramente clone este repositório com o seguinte comando:

<code>$ git clone git@bitbucket.org:lucasquearis/test-frontend.git</code>

Apos clonado o repositório, entre em sua pasta principal com o comando:

<code>$ cd test-frontend</code>

Inicie nosso "Back-end" com o comando:

<code>$ json-server --watch db.json -p 8080 -m lmd.js --routes routes.json </code>

E deixe esse terminal aberto, após isso, entre na pasta do front end com o comando:

<code>$ cd lucasquearis-prev-frontend-case</code>

Instale todas as dependências com o comando:

<code>$ npm install</code>

Após isso inicie a aplicação com o comando:

<code>$ npm start</code>

## <a name="architecture"></a> Arquitetura

![Imagem Arquitetura pastas](/Image/arquitetura.png)

A arquitetura do projeto está dividida em pastas referentes a caracteristicas dos conteúdos,

- Components - todos os componentes React da aplicação.
- context - todos os arquivos referentes aos contexts API's.
- functions - todas as funções auxiliares da aplicação.
- Hooks - hooks personalizados usados no projeto.
- Icons - icones usados na aplicação.
- Pages - todas a páginas da aplicação.
- tests - testes RTL da aplicação.

## <a name="testes"></a> Testes

Scripts utilizados:

- <code>$ npm test</code>: script que irá rodar todos os testes implementados na aplicação.
- <code>$ npm run lint</code>: script que irá percorrer todos os arquivos <code>.js</code> e <code>.jsx</code> da aplicação para verificar erros de sintaxe.
- <code>$ npm run coverage</code>: script que irá criar uma pasta coverage com arquivos de análise dos testes aplicados na aplicação.

## <a name="technologies"></a> Tecnologias utilizadas

- antd: "4.18.7" - biblioteca Ant Design
- ant-design/icons: "4.7.0" - icones da biblioteca Ant Design.
- axios: "0.26.0" - biblioteca para requisições HTTP.
- jwt-decode: "3.1.2" - biblioteca para decodificação HS256.
- prop-types: "15.8.1" - biblioteca para tipagem das props.
- react: "17.0.2" - versão do react.
- react-router-dom: "6.2.1" - biblioteca para manipulação de rotas das páginas.
- styled-components: "5.3.3" - biblioteca para estilização em componentes.
- uuid: "8.3.2" - biblioteca para gerar id únicos para keys dos maps.
- eslint: "8.9.0" - lint para padronização dos códigos.
- eslint-config-airbnb: "19.0.4" - Configuração do lint no modelo airbnb.
